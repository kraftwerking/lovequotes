//
//  KeyChainViewController.swift
//
//
//  Created by RJ Militante on 9/17/16.
//  Copyright (c) 2016 Kraftwerking. All rights reserved.
//

import UIKit
import GoogleMobileAds
import Social
import GameplayKit

class KeyChainViewController: UIViewController, GADBannerViewDelegate {

    @IBOutlet var bannerView: GADBannerView!
    @IBOutlet var menuButton:UIButton!
    @IBOutlet var quoteButton:UIButton!
    @IBOutlet var quotetext:UITextView!
    @IBOutlet var navigation: UIView!

    @IBOutlet var favButton:UIButton!
    @IBOutlet var viewBottom:UIView!

    @IBOutlet var lblAuthor:UILabel!
    @IBOutlet var lblCurDate:UILabel!
    @IBOutlet var imgBackground: UIImageView!

    // changed by ibrahim
    @IBOutlet weak var fbBtn: UIButton!
    @IBOutlet weak var twBtn: UIButton!
    @IBOutlet weak var uploadBtn: UIButton!
    @IBOutlet weak var likeBtn: UIButton!
    @IBOutlet weak var favBtn: UIButton!
    @IBOutlet weak var settingBtn: UIButton!

    @IBOutlet var keyChainView: UIView!

    @IBOutlet var viewBG:UIView!

    var arrayQuote : NSMutableArray = []
    var arrayColor : NSArray = ["#b95557","#ffffff","#0080ef","#ffee55","#fc7a3f","#a25d85","#808080","#41723d","#f571b5","#91685b"]

    var idxQuote:NSInteger=0
    var idxColor:NSInteger=0
    var randomQuoteID:NSInteger=0
    var isRandom:NSInteger=0

    var strProductId : NSString = ""

    var maxLimit:NSInteger=64
    var maxNumQts:NSInteger=3085 //max quotes in db, quotes in db should be random - 1
    var maxClicks:NSInteger=5

    var iMinSessions = 3
    var iTryAgainSessions = 6
    let appDelegate = UIApplication.shared.delegate as! AppDelegate // Create reference to our app delegate

    override func viewDidLoad() {
        super.viewDidLoad()
        Flurry.logEvent("Main view loaded");

    }

    override func viewWillAppear(_ animated: Bool) {


        self.revealViewController().panGestureRecognizer().isEnabled=false

        let screenSize: CGRect = UIScreen.main.bounds
        let screenHeight = screenSize.height
        let screenWidth = screenSize.width

        if screenHeight == 480.0 {

            viewBottom.frame=CGRect(x: 0, y: 458-88, width: 320, height: 60);
            bannerView.frame=CGRect(x: 0, y: 518-88, width: 320, height: 50);
            quotetext.frame=CGRect(x: 8 , y: 60, width: 304, height: 398-88);
        }
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad {
            print("this is ipad")

            let x1 = twBtn.frame.size.width
            let offset1 = fbBtn.frame.origin.x
            let pd1 = (screenWidth - ( x1*4 + offset1*2 ))/3

            let X_Position_tw = x1 + offset1 + pd1
            let Y_Position_tw = twBtn.frame.origin.y
            twBtn.frame.origin = CGPoint(x: X_Position_tw, y: Y_Position_tw)

            let X_Position_upload = x1*2 + offset1 + pd1*2
            let Y_Position_upload = uploadBtn.frame.origin.y
            uploadBtn.frame.origin = CGPoint(x: X_Position_upload, y: Y_Position_upload)

            let x2 = settingBtn.frame.size.width
            let offset2 = settingBtn.frame.origin.x
            let pd2 = (screenWidth - ( x2*4 + offset2*2 ))/3

            let X_Position_like = x2 + offset2 + pd2
            let Y_Position_like = likeBtn.frame.origin.y
            likeBtn.frame.origin = CGPoint(x: X_Position_like, y: Y_Position_like)

            let X_Position_fav = x2*2 + offset2 + pd2*2
            let Y_Position_fav = favBtn.frame.origin.y
            favBtn.frame.origin = CGPoint(x: X_Position_fav, y: Y_Position_fav)

            print(screenWidth )
            print("\(x2)" )
            print(offset1 )
            print(pd1 )
            print(X_Position_tw )
            print(X_Position_upload )
        }

        quotetext.layer.shadowColor = UIColor.black.cgColor
        quotetext.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        quotetext.layer.shadowOpacity = 1.0
        quotetext.layer.shadowRadius = 2.0
        quotetext.layer.backgroundColor = UIColor.clear.cgColor

        viewBottom.layer.shadowColor = UIColor.black.cgColor
        viewBottom.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        viewBottom.layer.shadowOpacity = 1.0
        viewBottom.layer.shadowRadius = 2.0
        viewBottom.layer.backgroundColor = UIColor.clear.cgColor

        navigation.layer.shadowColor = UIColor.black.cgColor
        navigation.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        navigation.layer.shadowOpacity = 1.0
        navigation.layer.shadowRadius = 2.0
        navigation.layer.backgroundColor = UIColor.clear.cgColor

        let  backgroundImg = UserDefaults.standard.integer(forKey: "idxRow") as NSInteger

        let str = NSString(format:"%d.jpg", backgroundImg+1)
        imgBackground.image = UIImage(named:str as String)

        isRandom=0;

        //        let defaults = NSUserDefaults.standardUserDefaults()
        //        defaults.setBool(true , forKey: "purchased")
        //
        //        defaults.synchronize()

        initializeAd()
        checkOnFullVersion()
        setCurrentDate()

        print(Utility.getPath("QuoteDB.db"))

        let del = (UIApplication.shared.delegate as! AppDelegate)
        let QuoteDB = FMDatabase(path: del.strDBpath as String)

        if (QuoteDB?.open())! {
            let querySQL = "SELECT * from Quote ORDER BY RANDOM()"
            let results:FMResultSet? = QuoteDB?.executeQuery(querySQL,
                                                             withArgumentsIn: nil)
            while results!.next() {
                var parameters = [String: AnyObject]()

                parameters = [
                    "ID":(results?.string(forColumn: "ID")!)! as AnyObject,
                    "QuoteText":(results?.string(forColumn: "QuoteText")!)! as AnyObject,
                    "QuoteAuthor":(results?.string(forColumn: "QuoteAuthor")!)! as AnyObject
                ]

                arrayQuote.add(parameters)
            }

            //print(arrayQuote);
            //print(arrayQuote.count);
            QuoteDB?.close()
            Flurry.logEvent("QuoteDB loaded");


        } else {
            print("Error opening QuoteDB")
            Flurry.logEvent("Error opening QuoteDB");

        }

        idxQuote = UserDefaults.standard.integer(forKey: "idxQuote") as NSInteger
        print("idxQuote: " + String(idxQuote))
        let todayQtTxt = getQuoteForIdx(idxQuote) as String;

        quoteButton.setTitle(getQuoteForIdx(idxQuote) as String, for: UIControlState())
        print("setting quote text for the day")
        quotetext.text = todayQtTxt
        alignTextVerticalInTextView(quotetext);
        lblAuthor.text=getAuthorForIdx(idxQuote) as String

        var parameters = [String: AnyObject]()
        parameters=arrayQuote.object(at: idxQuote) as! [String : AnyObject]

        let  quoteId=parameters["ID"] as! NSString

        if(isFavQuoteForId(quoteId.integerValue)) {
            favButton .setImage(UIImage(named:"Dislike"), for: UIControlState())
        }
        else {
            favButton .setImage(UIImage(named:"Like"), for: UIControlState())
        }

        let  val = UserDefaults.standard.integer(forKey: "idxColor") as NSInteger
        idxColor=val
        UserDefaults.standard.set(val, forKey: "idxColor")
        UserDefaults.standard.synchronize()

        //let color1 = colorWithHexString(arrayColor.objectAtIndex(val) as! NSString as String)

        //      self.view.backgroundColor=color1

        let date = UserDefaults.standard.object(forKey: "time")

        if((date == nil)) {
            print("Initial load of NSUserDefaults and notifications")

            let dateFormatter = DateFormatter()
            let date = Date()
            print("Date \(date)")

            dateFormatter.locale = NSLocale(localeIdentifier:"en_US_POSIX") as Locale!
            dateFormatter.dateFormat = "hh:mm a"
            let time=dateFormatter.date(from: "09:00 am")!
            print("Time \(time)")

            UserDefaults.standard.set(time, forKey: "time") //just stores alert time
            UserDefaults.standard.synchronize()

            UserDefaults.standard.set(true, forKey: "AlertsOn")
            UserDefaults.standard.synchronize()

            UserDefaults.standard.set("09:00 am", forKey: "strtime")
            UserDefaults.standard.synchronize()

            UserDefaults.standard.set(0, forKey: "interstitialAdClicks")
            UserDefaults.standard.synchronize()

            UIApplication.shared.cancelAllLocalNotifications()

            Flurry.logEvent("Sync notifications starting first time");

            syncNotifications(date)

        } else {
            let alertsOn:Bool = UserDefaults.standard.bool(forKey: "AlertsOn")
            Flurry.logEvent("Sync notifications starting");

            if(alertsOn == true) {
                let date = Date()
                print("Date \(date)")
                print("Sync notifications")
                UIApplication.shared.cancelAllLocalNotifications()
                syncNotifications(date)
            }
        }

        self.navigationController?.isNavigationBarHidden=true

        if revealViewController() != nil {

            menuButton.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: UIControlEvents.touchUpInside)

            revealViewController().rightViewRevealWidth = 150

            //            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }

        alignTextVerticalInTextView(quotetext);

    }

    override var prefersStatusBarHidden : Bool {
        return true;
    }

    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }

    func syncNotifications(_ stDate: Date) {

        let defaults = UserDefaults.standard

        if (defaults.bool(forKey: "purchased")){
            print("BEGIN Sync notifications")

            let calendar: Calendar = Calendar(identifier: Calendar.Identifier.gregorian)
            let formatter = DateFormatter()

            //print("maxLimit \(maxLimit)")
            //print("maxNumQts \(maxNumQts)")

            // set 64 notifications starting w idxQuote
            // date is the start of next 64 notifications
            var idxQuote = UserDefaults.standard.integer(forKey: "idxQuote")
            var date = stDate
            //print("idxQuote \(idxQuote)")
            //print("date \(date)")


            print("Number of notifications " + String(describing: UIApplication.shared.scheduledLocalNotifications?.count))

            //get alert hr min am
            let time = UserDefaults.standard.object(forKey: "time")
            let comp = (calendar as NSCalendar).components([.hour, .minute], from: time as! Date)
            let hour = comp.hour
            let minute = comp.minute

            //print("Hr \(hour)")
            //print("Min \(minute)")

            //set new alert hr min am
            date = (calendar as NSCalendar).date(bySettingHour: hour!, minute: minute!, second: 0, of: date, options: NSCalendar.Options.matchFirst)!
            //print("date \(date)")

            // create a corresponding local notification
            // same notification repeats daily

            print("Notifications being created")

            while (UIApplication.shared.scheduledLocalNotifications?.count)! < maxLimit{
                let qtTxt = getQuoteForIdx(idxQuote) as String;

                formatter.dateStyle = DateFormatter.Style.long
                formatter.timeStyle = .medium

                //print(idxQuote)
                //print(qtTxt)
                //print(formatter.stringFromDate(date))

                let notification = UILocalNotification()
                notification.alertBody = qtTxt // text that will be displayed in the notification
                notification.alertAction = "open" // text that is displayed after "slide to..." on the lock screen - defaults to "slide to view"
                notification.fireDate=date // todo item due date (when notification will be fired)
                notification.soundName = "magic.caf" // play default sound
                notification.userInfo = ["idxQuote": idxQuote] // assign a unique identifier to the notification so that we can retrieve it later
                //notification.repeatInterval = NSCalendarUnit.Day
                notification.applicationIconBadgeNumber =
                    UIApplication.shared.applicationIconBadgeNumber + 1
                //print("notification.fireDate \(notification.fireDate)")
                //print("notification.userInfo \(notification.userInfo)")

                UIApplication.shared.scheduleLocalNotification(notification)

                if(idxQuote < maxNumQts){
                    idxQuote = idxQuote + 1
                } else {
                    idxQuote = 0
                }

                let calendar = Calendar.current
                let tomorrowDate = calendar.date(byAdding: .day, value: 1, to: date)

                //print("tomorrowDate \(tomorrowDate)")

                date = tomorrowDate!
            }
            print("Number of notifications " + String(describing: UIApplication.shared.scheduledLocalNotifications?.count))
            //print(UIApplication.sharedApplication().scheduledLocalNotifications)
            let arrayOfLocalNotifications = UIApplication.shared.scheduledLocalNotifications
            for row in arrayOfLocalNotifications! {
                print("userInfo \(row.userInfo)")
                print("fireDate \(row.fireDate)")
                print("alertBody \(row.alertBody)")

            }

            print("idxQuote \(idxQuote)")

            print("END Sync notifications")
            Flurry.logEvent("Sync notifications purchased end");


        } else {

            var date = stDate
            print("BEGIN Sync notifications 2")
            print(date)

            let calendar: Calendar = Calendar(identifier: Calendar.Identifier.gregorian)

            //get alert hr min am
            let time = UserDefaults.standard.object(forKey: "time")
            let comp = (calendar as NSCalendar).components([.hour, .minute], from: time as! Date)
            let hour = comp.hour
            let minute = comp.minute

            //print("Hr \(hour)")
            //print("Min \(minute)")

            //set new alert hr min am
            date = (calendar as NSCalendar).date(bySettingHour: hour!, minute: minute!, second: 0, of: date, options: NSCalendar.Options.matchFirst)!

            // create a corresponding local notification
            let notification = UILocalNotification()
            notification.alertBody = "Your daily quote is here" // text that will be displayed in the notification
            notification.alertAction = "open" // text that is displayed after "slide to..." on the lock screen - defaults to "slide to view"
            notification.fireDate=date // todo item due date (when notification will be fired)
            notification.soundName = "magic.caf" // play default sound
            notification.userInfo = ["idxQuote": 0] // assign a unique identifier to the notification so that we can retrieve it later
            notification.repeatInterval = NSCalendar.Unit.day
            notification.applicationIconBadgeNumber =
                UIApplication.shared.applicationIconBadgeNumber + 1
            UIApplication.shared.scheduleLocalNotification(notification)

            print(UIApplication.shared.scheduledLocalNotifications)

            print("Number of notifications " + String(describing: UIApplication.shared.scheduledLocalNotifications?.count))
            //print(UIApplication.sharedApplication().scheduledLocalNotifications)
            let arrayOfLocalNotifications = UIApplication.shared.scheduledLocalNotifications
            for row in arrayOfLocalNotifications! {
                print("userInfo \(row.userInfo)")
                print("fireDate \(row.fireDate)")
                print("alertBody \(row.alertBody)")

            }

            print("END Sync notifications 2")
            Flurry.logEvent("Sync notifications not purchased end");


        }

    }

    func rateMe() {
        let neverRate = UserDefaults.standard.bool(forKey: "neverRate")
        var numLaunches = UserDefaults.standard.integer(forKey: "numLaunches") + 1

        if (!neverRate && (numLaunches == iMinSessions || numLaunches >= (iMinSessions + iTryAgainSessions + 1)))
        {
            showRateMe()
            numLaunches = iMinSessions + 1
        }
        UserDefaults.standard.set(numLaunches, forKey: "numLaunches")
    }

    func showRateMe() {
        let alert = UIAlertController(title: "Please rate us!", message: "If you enjoy using the Love Quotes App, please give us a good rating!", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Rate App 5 Stars!", style: UIAlertActionStyle.default, handler: { alertAction in
            UIApplication.shared.openURL(URL(string : "itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=id1157056025")!)
            alert.dismiss(animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "No Thanks", style: UIAlertActionStyle.default, handler: { alertAction in
            UserDefaults.standard.set(true, forKey: "neverRate")
            alert.dismiss(animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Maybe Later", style: UIAlertActionStyle.default, handler: { alertAction in
            alert.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }

    func buyMe() {
        let alert = UIAlertController(title: "Get Love Quotes on your lockscreen!", message: "Get quotes delivered directly to your lock screen! \n If you enjoy using the Love Quotes App, buy the full version to remove ads!", preferredStyle: UIAlertControllerStyle.alert)
        let action = UIAlertAction(title: "OK", style: .default) { (action) -> Void in
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.status=fromHome

            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "SettingsViewController")
            self.navigationController!.pushViewController(vc, animated: true)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default) { (action) -> Void in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(action)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }

    func checkOnFullVersion()  {
        let defaults = UserDefaults.standard

        if (defaults.bool(forKey: "purchased")){
            // Hide ads and don't show any ads
            self.bannerView.isHidden=true
            // update bottom view

            let screenSize: CGRect = UIScreen.main.bounds
            let screenHeight = screenSize.height
            let size_y = viewBottom.frame.size.height
            let y2 = screenHeight - size_y
            viewBottom.frame.origin = CGPoint(x: 0, y: y2)
        }
        else {
            self.bannerView.isHidden=false

        }
    }

    func setCurrentDate()  {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier:"en_US_POSIX") as Locale!

        let date = Date()

        dateFormatter.dateFormat = "MMM dd"
        print(dateFormatter.string(from: date))

        lblCurDate.text=dateFormatter.string(from: date)

        let storeddate = UserDefaults.standard.object(forKey: "date")

        if((storeddate == nil)) {

            UserDefaults.standard.set(lblCurDate.text, forKey: "date")
            UserDefaults.standard.synchronize()

            UserDefaults.standard.set(0, forKey: "idxQuote")
            UserDefaults.standard.synchronize()
        }
        else {
            if(storeddate as? String==lblCurDate.text) {
                // same day
            }
            else {
                // other day
                UserDefaults.standard.set(lblCurDate.text, forKey: "date")
                UserDefaults.standard.synchronize()

                var val = UserDefaults.standard.integer(forKey: "idxQuote") as NSInteger

                if(val<maxNumQts) {
                    val=val+1
                    if val % 2 == 0 {
                        rateMe()
                    } else {
                        if (UserDefaults.standard.bool(forKey: "purchased") == false){
                            buyMe()
                        }
                    }
                }
                else {
                    val=0
                    rateMe()
                }

                UserDefaults.standard.set(val, forKey: "idxQuote")
                UserDefaults.standard.synchronize()
            }
        }
    }

    func getAuthorForIdx(_ idx:NSInteger)  -> String  {

        var parameters = [String: AnyObject]()
        parameters=arrayQuote.object(at: idx) as! [String : AnyObject]
        return parameters["QuoteAuthor"] as! String;

    }

    func getQuoteForIdx(_ idx:NSInteger)  -> String  {

        var parameters = [String: AnyObject]()
        parameters=arrayQuote.object(at: idx) as! [String : AnyObject]

        var strTemp : NSString = parameters["QuoteText"] as! String as NSString
        strTemp = NSString(format:"%@\n\n-%@", strTemp,getAuthorForIdx(idx))

        return strTemp as String;
    }

    func isFavQuoteForId(_ QuoteID:NSInteger)  -> Bool  {

        let del = (UIApplication.shared.delegate as! AppDelegate)

        let QuoteDB = FMDatabase(path: del.strDBpath as String)

        if (QuoteDB?.open())! {

            let str = NSString(format:"SELECT * from Favorites where QuoteID='%d'",QuoteID)

            let querySQL = str

            let results:FMResultSet? = QuoteDB?.executeQuery(querySQL as String,
                                                             withArgumentsIn: nil)

            if results!.next() {
                QuoteDB?.close()
                return true
            }
            else {
                QuoteDB?.close()
                return false
            }
        } else {
        }

        return true
    }

    func initializeAd() {

        let req = GADRequest.init()
        //        req.testDevices=[ kGADSimulatorID ];
        //  self.bannerView.frame=CGRectMake(0, 518, 320, 50);

        self.bannerView.adUnitID="ca-app-pub-7856943859627690/7704304561";
        self.bannerView.rootViewController = self;
        self.bannerView.delegate = self
        self.bannerView.load(req)
    }


    /* func respondToSwipeGesture(gesture: UIGestureRecognizer) {

     if let swipeGesture = gesture as? UISwipeGestureRecognizer {

     switch swipeGesture.direction {
     case UISwipeGestureRecognizerDirection.Right:
     print("Swiped right")
     //     appdelegate.showMessage("Swiped Right")
     var val = NSUserDefaults.standardUserDefaults().integerForKey("idxQuote") as NSInteger

     if(idxQuote>0) {
     idxQuote -= 1
     quoteButton.setTitle(getQuoteForIdx(idxQuote) as String, forState: .Normal)
     quotetext.text = getQuoteForIdx(idxQuote) as String;
     alignTextVerticalInTextView(quotetext);

     lblAuthor.text=getAuthorForIdx(idxQuote) as String

     var parameters = [String: AnyObject]()
     parameters=arrayQuote.objectAtIndex(idxQuote) as! [String : AnyObject]

     let  quoteId=parameters["ID"] as! NSString

     if(isFavQuoteForId(quoteId.integerValue)) {
     favButton .setImage(UIImage(named:"Dislike"), forState: UIControlState.Normal)
     }
     else {
     favButton .setImage(UIImage(named:"Like"), forState: UIControlState.Normal)
     }
     }

     case UISwipeGestureRecognizerDirection.Down:
     print("Swiped down")

     if(idxColor<9) {
     idxColor += 1;
     let color1 = colorWithHexString(arrayColor.objectAtIndex(idxColor) as! NSString as String)

     NSUserDefaults.standardUserDefaults().setInteger(idxColor, forKey: "idxColor")
     NSUserDefaults.standardUserDefaults().synchronize()

     //self.view.backgroundColor=color1

     let defaults = NSUserDefaults.standardUserDefaults()

     if (defaults.boolForKey("purchased")){
     // Hide ads and don't show any ads
     }
     else {

     if (interstitial.isReady) {
     //  interstitial.presentFromRootViewController(self)
     }
     }

     }

     //   appdelegate.showMessage("Swiped down")

     case UISwipeGestureRecognizerDirection.Left:
     print("Swiped left")
     //    appdelegate.showMessage("Swiped left")

     let  val = NSUserDefaults.standardUserDefaults().integerForKey("idxQuote") as NSInteger

     if(idxQuote<val) {
     if(idxQuote<arrayQuote.count) {
     idxQuote += 1
     quoteButton.setTitle(getQuoteForIdx(idxQuote) as String, forState: .Normal)
     quotetext.text = getQuoteForIdx(idxQuote) as String;
     alignTextVerticalInTextView(quotetext);

     lblAuthor.text=getAuthorForIdx(idxQuote) as String

     var parameters = [String: AnyObject]()
     parameters=arrayQuote.objectAtIndex(idxQuote) as! [String : AnyObject]

     let  quoteId=parameters["ID"] as! NSString

     if(isFavQuoteForId(quoteId.integerValue)) {
     favButton .setImage(UIImage(named:"Dislike"), forState: UIControlState.Normal)
     }
     else {
     favButton .setImage(UIImage(named:"Like"), forState: UIControlState.Normal)

     }

     }
     else {
     }
     }

     case UISwipeGestureRecognizerDirection.Up:
     //   appdelegate.showMessage("Swiped up")

     if(idxColor>0) {
     idxColor -= 1;
     let color1 = colorWithHexString(arrayColor.objectAtIndex(idxColor) as! NSString as String)

     //   self.view.backgroundColor=color1

     NSUserDefaults.standardUserDefaults().setInteger(idxColor, forKey: "idxColor")
     NSUserDefaults.standardUserDefaults().synchronize()

     let defaults = NSUserDefaults.standardUserDefaults()

     if (defaults.boolForKey("purchased")){
     // Hide ads and don't show any ads
     }
     else {

     if (interstitial.isReady) {
     //   interstitial.presentFromRootViewController(self)
     }
     }

     }

     print("Swiped up")
     default:
     break
     }
     }
     } */

    @IBAction func favQuoteTapped(_ sender:UIButton!)
    {
        displayInterstitial()

        Flurry.logEvent("Fav quote button tapped");

        //let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate

        var parameters = [String: AnyObject]()

        print("isRandom \(isRandom)")
        print("idxQuote \(idxQuote)")

        if(isRandom==1) {
            print("add random quote to fav")

            parameters=arrayQuote.object(at: randomQuoteID) as! [String : AnyObject]

        }
        else {
            print("add main quote to fav")

            parameters=arrayQuote.object(at: idxQuote) as! [String : AnyObject]

        }

        let  quoteId=parameters["ID"] as! NSString
        print("parameters \(parameters)")
        print("quoteId \(quoteId)")


        if(isFavQuoteForId(quoteId.integerValue)) {
            favButton .setImage(UIImage(named:"Like"), for: UIControlState())
            // delete query

            let del = (UIApplication.shared.delegate as! AppDelegate)

            let QuoteDB = FMDatabase(path: del.strDBpath as String)

            if (QuoteDB?.open())! {
                let str = NSString(format:"delete from Favorites where QuoteID=%d", quoteId.integerValue)

                let result = QuoteDB?.executeUpdate(str as String,
                                                    withArgumentsIn: nil)
                print(result)
            }

            QuoteDB?.close()
        }
        else {
            favButton .setImage(UIImage(named:"Dislike"), for: UIControlState())
            // insert query

            let del = (UIApplication.shared.delegate as! AppDelegate)

            let QuoteDB = FMDatabase(path: del.strDBpath as String)

            if (QuoteDB?.open())! {
                let  val = UserDefaults.standard.integer(forKey: "idxColor") as NSInteger

                let str = NSString(format:"INSERT INTO Favorites (QuoteID,ColorIndex) VALUES (%d,%d)", quoteId.integerValue,val)

                let result = QuoteDB?.executeUpdate(str as String,
                                                    withArgumentsIn: nil)
                print(result)
            }

            QuoteDB?.close()
        }

    }



    @IBAction func favTapped(_ sender:UIButton!)
    {

        Flurry.logEvent("Fav tapped");

        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.status=fromHome
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ActivityLogVC")
        self.navigationController!.pushViewController(vc, animated: true)

        //        let rvc:SWRevealViewController = self.revealViewController() as SWRevealViewController
        //
        //        rvc.pushFrontViewController(vc, animated: true)

        //        let alert = UIAlertController(title: "View", message: "Please Select an Option", preferredStyle: .ActionSheet)
        //
        //        alert.addAction(UIAlertAction(title: "Favorites", style: .Default , handler:{ (UIAlertAction)in
        //            print("Favorites")
        //
        //            let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        //            appdelegate.status=fromHome
        //
        //            let storyboard = UIStoryboard(name: "Main", bundle: nil)
        //            let vc = storyboard.instantiateViewControllerWithIdentifier("ActivityLogVC")
        //            let rvc:SWRevealViewController = self.revealViewController() as SWRevealViewController
        //            rvc.pushFrontViewController(vc, animated: true)
        //        }))
        //
        //        alert.addAction(UIAlertAction(title: "Background", style: .Default , handler:{ (UIAlertAction)in
        //            print("Background")
        //
        //            let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        //            appdelegate.status=fromHome
        //
        //            let storyboard = UIStoryboard(name: "Main", bundle: nil)
        //            let vc = storyboard.instantiateViewControllerWithIdentifier("GalleryVC")
        //            let rvc:SWRevealViewController = self.revealViewController() as SWRevealViewController
        //            rvc.pushFrontViewController(vc, animated: true)
        //
        //        }))
        //
        //        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler:{ (UIAlertAction)in
        //            print("Cancel")
        //
        //        }))
        //
        //        self.presentViewController(alert, animated: true, completion: {
        //            print("completion block")
        //        })
    }

    @IBAction func randomQuoteTapped(_ sender:UIButton!)
    {
        Flurry.logEvent("Random quote button tapped");

        isRandom=1;

        //let MIN : UInt32 = 0
        //maxLimit
        let nQuote = UInt32(maxNumQts   )                      // Convert to Uint32
        randomQuoteID = Int(arc4random_uniform(nQuote))          // Range between 0 - nQuote

        print("setting random quote text")
        quotetext.text = getQuoteForIdx(randomQuoteID) as String;

        alignTextVerticalInTextView(quotetext);

        displayRandomInterstitial()

    }


    func displayRandomInterstitial() {
        let defaults = UserDefaults.standard

        var  clicks = UserDefaults.standard.integer(forKey: "interstitialAdClicks") as NSInteger

        if (defaults.bool(forKey: "purchased")){
            //no ads
        } else {
            if(clicks < maxClicks) {
                clicks = clicks + 1
                UserDefaults.standard.set(clicks, forKey: "interstitialAdClicks")
                UserDefaults.standard.synchronize()
            } else {
                clicks = 1
                UserDefaults.standard.set(clicks, forKey: "interstitialAdClicks")
                UserDefaults.standard.synchronize()
                displayInterstitial()
            }
        }

    }

    func displayInterstitial() {
        let defaults = UserDefaults.standard

        if (defaults.bool(forKey: "purchased") == false){
            if (appDelegate.myInterstitial?.isReady)! {
                print("Ad is ready")
                appDelegate.myInterstitial?.present(fromRootViewController: self)
            } else {
                print("Ad wasn't ready")
            }
        }

    }

    func getRandomQuote() -> NSString
    {
        isRandom=1;

        //let MIN : UInt32 = 0
        //maxLimit
        let nQuote = UInt32(maxNumQts   )                      // Convert to Uint32
        randomQuoteID = Int(arc4random_uniform(nQuote))          // Range between 0 - nQuote

        let qtTxt = getQuoteForIdx(randomQuoteID) as String;

        return qtTxt as NSString
    }

    @IBAction func shareQuoteTapped(_ sender:UIButton!)
    {
        Flurry.logEvent("Share quote button tapped");

        let shareText = NSString(format:"Sharing the quote from Love Quotes App :\n%@\nby %@ \n#quotes #love #lovequotes #lovequotesapp \nhttps://itunes.apple.com/us/app/daily-love-quotes-sayings/id1168496565?ls=1&mt=8", (quoteButton.titleLabel?.text)!,lblAuthor.text!)

        let vc = UIActivityViewController(activityItems: [shareText], applicationActivities: [])
        present(vc, animated: true, completion: nil)

    }

    @IBAction func shareToFacebook(_ sender: AnyObject) {
        Flurry.logEvent("Share quote to FB button tapped");

        let shareToFacebook : SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
        print("Share to facebook \(quotetext.text)")
        //shareToFacebook.setInitialText(quotetext.text)

        let alert: UIAlertView = UIAlertView(title: "Copy Quote", message: "Share today's quote on Facebook!\n\nYou can just PASTE for a preset message.", delegate: nil, cancelButtonTitle: "Cancel");
        alert.show()
        // Delay the dismissal by 5 seconds
        let delay = 4.0 * Double(NSEC_PER_SEC)
        let time = DispatchTime.now() + Double(Int64(delay)) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: time, execute: {
            alert.dismiss(withClickedButtonIndex: -1, animated: true)
            let pasteboard: UIPasteboard = UIPasteboard.general
            pasteboard.string = self.quotetext.text + "\n#quotes #love #lovequotes #lovequotesapp \nhttps://itunes.apple.com/us/app/daily-love-quotes-sayings/id1168496565?ls=1&mt=8";
            print("pasteboard.string \(pasteboard.string)")

        })

        self.present(shareToFacebook, animated: true, completion: nil)
    }

    @IBAction func shareToTwitter(_ sender: AnyObject) {
        Flurry.logEvent("Share quote to Twitter button tapped");

        let shareToTwitter : SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
        shareToTwitter.setInitialText(quotetext.text + "\n#quotes #love #lovequotes #lovequotesapp https://itunes.apple.com/us/app/daily-love-quotes-sayings/id1168496565?ls=1&mt=8")
        self.present(shareToTwitter, animated: true, completion: nil)
    }

    @IBAction func shareButtonClicked(_ sender: AnyObject) {
        Flurry.logEvent("Share button tapped");


        let textToShare = quotetext.text + "\n#quotes #love #lovequotes #lovequotesapp \nhttps://itunes.apple.com/us/app/daily-love-quotes-sayings/id1168496565?ls=1&mt=8"

        // 1.
        // Create and initialize a UIAlertController instance.
        //
        let alertController = UIAlertController(title: nil,
                                                message: nil,
                                                preferredStyle: .actionSheet)

        // 2.
        // Initialize the actions to show along with the alert.
        //
        let copyAction = UIAlertAction(title:"Copy Quote to clipboard",
                                       style: .default) { (action) -> Void in
                                        let pasteboard: UIPasteboard = UIPasteboard.general
                                        pasteboard.string = self.quotetext.text;
        }

        let defaultAction = UIAlertAction(title:"Share Inspiring Quote",
                                          style: .default) { (action) -> Void in
                                            // We have contents so display the share sheet

                                            let activityViewController = UIActivityViewController(activityItems: [textToShare as NSString], applicationActivities: nil)

                                            if let button = sender as? UIButton {
                                                activityViewController.popoverPresentationController?.sourceView = button
                                                activityViewController.popoverPresentationController?.sourceRect = button.bounds
                                            }

                                            self.present(activityViewController, animated: true, completion: {})


        }

        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            // ...
        }

        // 3.
        // Tell the alertController about the actions we want it
        // to present.
        //
        alertController.addAction(copyAction)
        alertController.addAction(defaultAction)
        alertController.addAction(cancelAction)


        if let button = sender as? UIButton {
            alertController.popoverPresentationController?.sourceView = button
            alertController.popoverPresentationController?.sourceRect = button.bounds
        }

        // 4.
        // Present the alert controller and associated actions.
        //
        self.present(alertController, animated: true, completion: nil)

    }

    func displayShareSheet(shareContent:String, btn: UIButton) {
        let activityViewController = UIActivityViewController(activityItems: [shareContent as NSString], applicationActivities: nil)
        present(activityViewController, animated: true, completion: {})
    }

    @IBAction func infoTapped(_ sender:UIButton!)
    {
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.status=fromHome

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "InfoViewController")

        self.navigationController!.pushViewController(vc, animated: true)
        //            let rvc:SWRevealViewController = self.revealViewController() as SWRevealViewController
        //            rvc.pushFrontViewController(vc, animated: true)
    }

    @IBAction func settingsTapped(_ sender:UIButton!)
    {
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.status=fromHome

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SettingsViewController")
        self.navigationController!.pushViewController(vc, animated: true)

        //        let rvc:SWRevealViewController = self.revealViewController() as SWRevealViewController
        //        rvc.pushFrontViewController(vc, animated: true)
    }
}


func alignTextVerticalInTextView(_ textView :UITextView) {

    let size = textView.sizeThatFits(CGSize(width: textView.bounds.width, height: CGFloat(MAXFLOAT)))

    var topoffset = (textView.bounds.size.height - size.height * textView.zoomScale) / 2.0
    topoffset = topoffset < 0.0 ? 0.0 : topoffset

    textView.contentOffset = CGPoint(x: 0, y: -topoffset)
}



func colorWithHexString (_ hex:String) -> UIColor {

    var cString = hex.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).uppercased()

    if (cString.hasPrefix("#")) {
        cString = (cString as NSString).substring(from: 1)
    }

    if (cString.characters.count != 6) {
        return UIColor.gray
    }

    let rString = (cString as NSString).substring(to: 2)
    let gString = ((cString as NSString).substring(from: 2) as NSString).substring(to: 2)
    let bString = ((cString as NSString).substring(from: 4) as NSString).substring(to: 2)

    var r:CUnsignedInt = 0, g:CUnsignedInt = 0, b:CUnsignedInt = 0;
    Scanner(string: rString).scanHexInt32(&r)
    Scanner(string: gString).scanHexInt32(&g)
    Scanner(string: bString).scanHexInt32(&b)


    return UIColor(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: CGFloat(1))
}
